/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author elias
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(entities.service.EquipoFacadeREST.class);
        resources.add(entities.service.ProyectoFacadeREST.class);
        resources.add(entities.service.RolFacadeREST.class);
        resources.add(entities.service.SprintFacadeREST.class);
        resources.add(entities.service.TareaFacadeREST.class);
        resources.add(entities.service.UserhistoriesFacadeREST.class);
        resources.add(entities.service.UsuarioFacadeREST.class);
    }
    
}
